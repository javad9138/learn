import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';

export class App extends Component {
    render() {
        return (
            <View>
              <Text>
                Hello World!
              </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 25,
        marginHorizontal: 16,
    },
});

export default App
